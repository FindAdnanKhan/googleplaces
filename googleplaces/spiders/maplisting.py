import scrapy
from scrapy_selenium import SeleniumRequest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import time

# Custom logic to cover all type of listings near given location
listing_type = ["accounting", "airport", "amusement_park", "aquarium", "art_gallery", "atm", "bakery", "bank", "bar", "beauty_salon", "bicycle_store", "book_store", "bowling_alley", "bus_station", "cafe", "campground", "car_dealer", "car_rental", "car_repair", "car_wash", "casino", "cemetery", "church", "city_hall", "clothing_store", "convenience_store", "courthouse", "dentist", "department_store", "doctor", "drugstore", "electrician", "electronics_store", "embassy", "fire_station", "florist", "funeral_home", "furniture_store", "gas_station", "gym", "hair_care", "hardware_store", "hindu_temple", "home_goods_store", "hospital", "insurance_agency", "jewelry_store", "laundry", "lawyer", "library", "light_rail_station", "liquor_store", "local_government_office", "locksmith", "lodging", "meal_delivery", "meal_takeaway", "mosque", "movie_rental", "movie_theater", "moving_company", "museum", "night_club", "painter", "park", "parking", "pet_store", "pharmacy", "physiotherapist", "plumber", "police", "post_office", "primary_school", "real_estate_agency", "restaurant", "roofing_contractor", "rv_park", "school", "secondary_school", "shoe_store", "shopping_mall", "spa", "stadium", "storage", "store", "subway_station", "supermarket", "synagogue", "taxi_stand", "tourist_attraction", "train_station", "transit_station", "travel_agency", "university", "veterinary_care", "zoo"]

# Input locations you can change it
find_nearby = ["jail road lahore", "model town loahore"]

# A logic to genrate links to get all type of nearby locations.
list_url = ["https://www.google.com/maps/search/" + list_type + "+near+" + nearby.strip().replace(" ", "+") + "/" for nearby in find_nearby for list_type in listing_type ]


class MaplistingSpider(scrapy.Spider):
    name = 'maplisting'
    allowed_domains = ['*']

    def start_requests(self):
        if list_url:
            url = list_url.pop()
            yield SeleniumRequest(url=url, callback=self.parse)

    def parse(self, response):
        '''
        limit variable used to limit the pagination coverage.
        e.g self.limit 2 cover two pages and its enough
            to cover nearby location.
        '''
        self.limit = 2
        try:
            WebDriverWait(response.meta["driver"], 60).until(
                EC.visibility_of_element_located((
                    By.CLASS_NAME, "section-result")))
        except:
            pass

        section_results = response.meta[
            "driver"].find_elements_by_css_selector(".section-result")
        while True:
            for i, result in enumerate(section_results):
                try:
                    WebDriverWait(response.meta["driver"], 60).until(
                        EC.visibility_of_element_located((
                            By.CLASS_NAME, "section-result")))
                except:
                    time.sleep(60)
                try:
                    response.meta[
                        "driver"].find_elements_by_css_selector(
                            ".section-result")[i].click()
                except:
                    time.sleep(30)
                try:
                    response.meta[
                        "driver"].find_elements_by_css_selector(
                            ".section-result")[i].click()
                except:
                    continue
                try:
                    WebDriverWait(response.meta["driver"], 60).until(
                        EC.visibility_of_element_located((
                            By.CLASS_NAME, "section-result-title")))
                except:
                    pass
                results = dict()

                try:
                    WebDriverWait(response.meta["driver"], 60).until(
                        EC.visibility_of_element_located((
                            By.CLASS_NAME, "section-hero-header-title-title")))
                except:
                    time.sleep(60)
                    pass
                try:
                    results["title"] = response.meta[
                        "driver"].find_element_by_css_selector(
                            ".section-hero-header-title-title").text
                except:
                     results["title"] = ''   
                try:
                    results["subtitle"] = response.meta[
                        "driver"].find_element_by_css_selector(
                            ".section-hero-header-title-subtitle").text
                except:
                    pass
                try:
                    results["category"] = response.meta[
                        "driver"].find_element_by_css_selector(
                            'button[jsaction="pane.rating.category"]').text
                except:
                    results["category"] = ''
                try:
                    results["star_display"] = response.meta[
                        "driver"].find_element_by_css_selector(
                            '.section-star-display').text
                except:
                    results["star_display"] = ""
                yield results
                try:
                    back_button = response.meta[
                        "driver"].find_element_by_css_selector(
                            ".section-back-to-list-button")
                except:
                    continue
                try:
                    back_button.click()
                except:
                    response.meta["driver"].execute_script(
                        "arguments[0].click();", back_button)

            try:
                WebDriverWait(response.meta["driver"], 60).until(
                    EC.visibility_of_element_located((
                        By.CLASS_NAME, "n7lv7yjyC35__button")))
            except:
                pass
            if response.meta["driver"].find_elements_by_css_selector(
                ".n7lv7yjyC35__button")[1].get_attribute(
                    "disabled") or self.limit == 1:
                if list_url:
                    try:
                        url = list_url.pop()
                    except:
                        return
                    response.meta["driver"].get(url)
                    self.limit = 2
            else:
                self.limit = self.limit - 1
                response.meta["driver"].find_elements_by_css_selector(
                    ".n7lv7yjyC35__button")[1].click()
                time.sleep(60)
