# README #

Scrapy runspider maplisting.py -o OutputFileName.filetype

### What is this repository for? ###

* The repo used to scrape nearby places from google map.
* The spider take locations as a input and scrape nearby location from google map. Input location can set in 
* 0.9 version (Beta)
* (https://FindAdnanKhan@bitbucket.org/FindAdnanKhan/googleplaces.git)

### How do I get set up? ###

* Install python version 3.7.6 and follow below commands one by one
* pip install requirements.txt
* download seleinum driver and set configuration in googleplaces/settings.py.

* SELENIUM_DRIVER_NAME = 'firefox'
* SELENIUM_DRIVER_EXECUTABLE_PATH = "googleplaces//googleplaces//geckodriver.exe"
* SELENIUM_DRIVER_ARGUMENTS=['-headless']


### Who do I talk to? ###

* The code write by Adnan (AdiKhanBloch@gmail.com)
* Skype https://join.skype.com/invite/lFMoRnGusmDg